const path = require("path");

module.exports = {
  entry: {
    index: ["./src/app.jsx"]
  },
  output: {
    filename: "bundle.js",
    path: path.join(__dirname, "/dist")
  },
  module: {
    rules: [
      {
        test: /.jsx$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
          options: { presets: ["@babel/preset-react", "@babel/preset-env"] }
        }
      }
    ]
  },
  devServer: {
    contentBase: path.join(__dirname, "dist"),
    compress: true,
    port: 9000
  }
};
