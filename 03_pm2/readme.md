
# Install pm2 
```
npm i pm2 -g
```

# Node run
```sh
node index.js
```

# PM2 run (cluster : 4)
```sh
pm2 start -i 4 --name server index.js
```


# Reference
 - https://larrylu.blog/nodejs-pm2-cluster-455ffbd7671