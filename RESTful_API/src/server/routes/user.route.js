import express from 'express';
import validate from 'express-validation';
import UserController from '../controllers/user.controller';
import paramValidation from '../../config/param-validation';

const router = express.Router();

router.route('/').get(UserController.getAll);
router.route('/').post(validate(paramValidation.createUser), UserController.create);
router.route('/:user_id').put(UserController.update);
router.route('/:user_id').delete(UserController.destory);
router.route('/login').post(UserController.login);

export default router;
