import ArticleModule from '../modules/article.module';

/**
 * Add new article data
 * @param {JSON} req json input article table data
 * @param {JSON} res json response
 */
const create = (req, res) => {
    // 取得新增參數
    const insertValues = req.body;
    ArticleModule.create(insertValues).then((result) => {
        return res.send(result); // 成功回傳result結果
    }).catch((err) => {
        return res.send(err); // 失敗回傳錯誤訊息
    });
};

/** Get all article data  */
const getAll = (req, res) => {
    ArticleModule.getAll().then((result) => {
        res.send(result); // 成功回傳result結果
    }).catch((err) => { return res.send(err); }); // 失敗回傳錯誤訊息
};

/** Update article data */
const update = (req, res) => {
    // 取得url params : article_id
    const articleId = req.params.article_id;
    const insertValues = req.body;

    ArticleModule.update(insertValues, articleId).then((result) => {
        res.send(result);
    }).catch((err) => { return res.send(err); });
};

/** Delete article data */
const destory = (req, res) => {
    // 取得url params : article_id
    const articleId = req.params.article_id;

    ArticleModule.destroy(articleId).then((result) => {
        res.send(result);
    }).catch((err) => { return res.send(err); });
};

/*  Article GET JWT取得個人文章  */
const getPersonalArticle = (req, res) => {
    ArticleModule.personalArticle(req.token).then((result) => {
        res.send(result); // 成功回傳result結果
    }).catch((err) => { return res.status(401).send(err); }); // 失敗回傳錯誤訊息
};

export default {
    create,
    getAll,
    update,
    destory,
    getPersonalArticle
};
