https://ithelp.ithome.com.tw/articles/10193299

1. 建立新資料夾並初始化專案
```
yarn init -y
```

2. 安裝 Webpack 與 babel
```
yarn add -D webpack webpack-node-externals

yarn add -D babel-loader @babel/core @babel/preset-env babel-plugin-transform-object-rest-spread 
```
> babel-core 目前只有到6.x版
> @babel/core 目前到7.x版
> 若在執行webpack -w 時出現`babel-loader@8 requires Babel 7.x`的錯誤
> 表示使babel-loader最新版時, 會需要babel core 7.x版


3. 建立設定檔

參考 webpack.config.js 檔案

4. 建立package.json快捷鍵
```json
"scripts": {
    "build": "webpack -w",
    "build:prod": "webpack -p",
    "start": "nodemon dist/index.bundle.js"
}
```

使用方法：  
```
yarn build
yarn build:prod
yarn start
```

5. 安裝nodemon
```sh
# 全域安裝
yarn global add nodemon

# 安裝於專案
yarn add -D nodemon 
```


6. 建立一個檔案並測試

建立 src/index.js 並撰寫簡單的nodejs code  

開啟一個terminal, 使用 `yarn build`  
`webpack -w` 的 w 代表的是 watch 就是值持續監聽有變動時會同步更新  

開啟另一個terminal, 使用 `yarn start`
啟動 `nodemon dist/index.bundle.js`  
也就是執行打包後的檔案  

若要發行部屬到測試或正式環境上, 則使用 `yarn build:prod`